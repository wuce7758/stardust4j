#Stardust4J

Stardust是一个微服务架构的一个简单实现。Stardust4J是JAVA版客户端和服务端的实现。


[http://www.cnblogs.com/loogn/p/6664594.html](http://www.cnblogs.com/loogn/p/6664594.html)
 
[.NET版](http://git.oschina.net/loogn/Stardust) 
  

##Service:
web.xml
```
<web-app>
    <filter>
        <filter-name>StardustFilter</filter-name>
        <filter-class>net.loogn.stardust.server.StardustFilter</filter-class>
        <init-param>
            <param-name>configClass</param-name>
            <param-value>net.loogn.stardust.webnode.MyStardustConfig</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>StardustFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
</web-app>
```
继承StardustConfig配置服务节点
```
public class MyStardustConfig extends StardustConfig {

    public void setConfig(ConfigSetter setter) {
        setter.setVersion("1.0")
                .setServiceName("node2")
                .setAddress("127.0.0.1:8086")
                .setConfigCenterUrl("http://localhost:85");
    }

    public void addService(ServiceAddor addor) {
        addor.add(UserService.class); //添加定义服务的类
    }
}
```
编写服务类
```
@StardustName("user")
public class UserService {
    //可以没有参数
    @StardustName("hello")
    public String GetStr() {
        return "Hello UserService";
    }
    
    //可以有一个String参数接收JSON数据
    public User UpdateUser(String json) {
        User user = JSON.parseObject(json, User.class);
        System.out.println("反序列化：" + JSON.toJSONString(user));
        user.setName("Update Name");
        user.setAddTime(new Date());
        user.setId(22);
        return user;
    }
}
```


##Client:
```
public class Program {
    public static void main(String[] args) throws InterruptedException {
        StardustClient.setConfigCenterUrl("http://localhost:85"); //运行时初始化一次
        try {
            StardustClient node1Client = new StardustClient("node1", "1.2");
            User user = new User();
            user.setName("Update Name");
            user.setAddTime(new Date());
            user.setId(22);

            String json = node1Client.invoke("user", "UpdateUser", user); //返回JSON字符串
            
            User result = JSON.parseObject(json, User.class);
            System.out.println(JSON.toJSONString(result));
        } catch (Exception exp) {
            System.out.println(exp.getMessage());
        }
    }
}
```