import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import net.loogn.stardust.client.StardustClient;

import java.util.*;

/**
 * Created by Administrator on 2017/4/6.
 */
class User {
    private int id;
    private String name;
    private String msg;

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    private Date addTime;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}

public class Program {
    public static void main(String[] args) throws InterruptedException {
        StardustClient.setConfigCenterUrl("http://localhost:85"); //运行时初始化一次
        try {
            StardustClient node1Client = new StardustClient("node1", "1.2");
            User user = new User();
            user.setName("Update Name");
            user.setAddTime(new Date());
            user.setId(22);

            String json = node1Client.invoke("user", "UpdateUser", user); //返回JSON字符串

            User result = JSON.parseObject(json, User.class);
            System.out.println(JSON.toJSONString(result));
        } catch (Exception exp) {
            System.out.println(exp.getMessage());
        }
    }
}
