package net.loogn.stardust.client;

import com.alibaba.fastjson.JSON;
import net.loogn.stardust.common.model.ServerNodeModel;
import net.loogn.stardust.common.utils.HttpHelper;

import java.io.IOException;

/**
 * Created by Administrator on 2017/4/6.
 */
public class StardustClient {
    private static String configCenterUrl;

    public static String getConfigCenterUrl() {
        return configCenterUrl;
    }

    public static void setConfigCenterUrl(String configCenterUrl) {
        StardustClient.configCenterUrl = configCenterUrl;
    }

    private ServerNodeModel node;

    public ServerNodeModel getNode() {
        return node;
    }

    public StardustClient(String serviceName, String version) throws Exception {
        if (configCenterUrl == null || configCenterUrl.length() == 0) {
            new Exception("请先设置配置中心地址：ConfigCenterUrl").printStackTrace();
        }
        node = NodeManager.getNode(serviceName, version);
    }

    public String invoke(String controllerName, String actionName, Object objParams) throws IOException {
        return invoke(controllerName, actionName, objParams, 6000);
    }

    public String invoke(String controllerName, String actionName, Object objParams, int timeout) throws IOException {
        String jsonParams = "";
        if (objParams != null) {
            jsonParams = JSON.toJSONString(objParams);
        }
        return invoke(controllerName, actionName, jsonParams, timeout);
    }


    public String invoke(String controllerName, String actionName, String jsonParams) throws IOException {
        return invoke(controllerName, actionName, jsonParams, 6000);
    }

    public String invoke(String controllerName, String actionName, String jsonParams, int timeout) throws IOException {
        if (jsonParams == null) {
            jsonParams = "";
        }
        String url = node.getServiceUrl(controllerName, actionName);
        String json = null;
        json = HttpHelper.postJsonToUrl(url, jsonParams, timeout);
        return json;
    }
}
