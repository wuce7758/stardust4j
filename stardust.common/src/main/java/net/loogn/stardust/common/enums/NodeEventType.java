package net.loogn.stardust.common.enums;

/**
 * Created by Administrator on 2017/4/6.
 */
public enum NodeEventType {
    Useless(0),
    Register(1),
    Logout(2),
    Update(3),
    Delete(4);

    private int typeId;

    NodeEventType(int i) {
        typeId = i;
    }
}
