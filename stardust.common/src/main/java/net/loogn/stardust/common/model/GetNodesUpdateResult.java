package net.loogn.stardust.common.model;

import java.util.List;

/**
 * Created by Administrator on 2017/4/6.
 */
public class GetNodesUpdateResult {
    private long maxEventId;
    private List<NodeEventModel> eventList;

    public long getMaxEventId() {
        return maxEventId;
    }

    public void setMaxEventId(long maxEventId) {
        this.maxEventId = maxEventId;
    }

    public List<NodeEventModel> getEventList() {
        return eventList;
    }

    public void setEventList(List<NodeEventModel> eventList) {
        this.eventList = eventList;
    }
}
