package net.loogn.stardust.common.model;

import net.loogn.stardust.common.enums.NodeEventType;

import java.util.Date;

/**
 * Created by Administrator on 2017/4/6.
 */
public class NodeEventModel {
    private long id;
    private long serverNodeId;
    private ServerNodeModel serverNode;
    private NodeEventType eventType;
    private Date triggerTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getServerNodeId() {
        return serverNodeId;
    }

    public void setServerNodeId(long serverNodeId) {
        this.serverNodeId = serverNodeId;
    }

    public ServerNodeModel getServerNode() {
        return serverNode;
    }

    public void setServerNode(ServerNodeModel serverNode) {
        this.serverNode = serverNode;
    }

    public NodeEventType getEventType() {
        return eventType;
    }

    public void setEventType(NodeEventType eventType) {
        this.eventType = eventType;
    }

    public Date getTriggerTime() {
        return triggerTime;
    }

    public void setTriggerTime(Date triggerTime) {
        this.triggerTime = triggerTime;
    }
}
