package net.loogn.stardust.webnode.services;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import net.loogn.stardust.server.StardustName;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/4/11.
 */
@StardustName("user")
public class UserService {
    //可以没有参数
    @StardustName("hello")
    public String GetStr() {
        return "Hello UserService";
    }

    //可以有一个String参数接收JSON数据
    public User UpdateUser(String json) {
        User user = JSON.parseObject(json, User.class);
        System.out.println("反序列化：" + JSON.toJSONString(user));
        user.setName("Update Name");
        user.setAddTime(new Date());
        user.setId(22);
        return user;
    }
}

class User {
    private int id;
    private String name;
    private Date addTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
